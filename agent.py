from decorator import check_access


class BaseUser:
	def __init__(self, username, password, first_name, last_name, email, **kwargs):
		self.username = username
		self.password = password
		self.first_name = first_name
		self.last_name = last_name
		self.email = email
		super().__init__(**kwargs)
	
	@classmethod
	def prompt(cls):
		username = input("please enter username:")
		password = input("please enter password:")
		first_name = input("please enter first_name:")
		last_name = input("please enter last_name:")
		email = input("please enter email:")
		result = {
			"username": username, "password": password, "first_name": first_name, "last_name": last_name, "email": email
		}
		return result


class SuperVisor(BaseUser):
	agent_list = list()
	deals_list = list()
	properties_list = list()
	
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
	
	@staticmethod
	def create_agent():
		agent_data = Agent.prompt()
		agent = Agent(**agent_data)
		return agent
	
	@classmethod
	def agent_data(cls):
		tmp = []
		for agent in cls.agent_list:
			tmp.append(agent.serializer())
		return tmp
	
	@classmethod
	def search(cls, username):
		for agent in cls.agent_list:
			if agent.username == username:
				return agent
		return None


class Agent(BaseUser):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.__has_access = False
		self.properties_list = list()
		self.deals_list = list()
		SuperVisor.agent_list.append(self)
	
	def check_password(self, password):
		check = bool(self.password == password)
		if check:
			self.__has_access = True
		return check
	
	def has_access(self):
		return self.has_access
	
	def serializer(self):
		data = self.__dict__
		data.pop("properties_list")
		data.pop("deals_list")
		data.pop("_Agent__has_access")
		return data
	
	@classmethod
	def prompt(cls):
		return BaseUser.prompt()
	
	@check_access
	def creat_deal(self):
		print("creat deal touched")
		pass
