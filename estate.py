from agent import SuperVisor


class BaseProperty:
	def __init__(self, area, room_number, parking, address, **kwargs):
		super().__init__(**kwargs)
		self.area = area
		self.room_number = room_number
		self.parking = parking
		self.address = address
		SuperVisor.properties_list.append(self)
	
	@classmethod
	def prompt(cls):
		area = input("please enter area :")
		room_number = input("please enter number of rooms :")
		parking = input("please enter parking number :")
		address = input("please enter address:")
		result = {"area": area, "room_number": room_number, "parking": parking, "address": address}
		return result


class Apartment(BaseProperty):
	def __init__(self, floor, elevator, balcony, **kwargs):
		self.floor = floor
		self.elevator = elevator
		self.balcony = balcony
		super().__init__(**kwargs)
	
	@classmethod
	def prompt(cls):
		floor = input("which floor:")
		elevator = input("Does it have an elevator?")
		balcony = input("Does it have a balcony?")
		result = {"floor": floor, "elevator": elevator, "balcony": balcony}
		result.update(BaseProperty.prompt())
		return result


class House(BaseProperty):
	def __init__(self, pool, yard, **kwargs):
		super().__init__(**kwargs)
		self.pool = pool
		self.yard = yard
	
	@classmethod
	def prompt(cls):
		pool = input("Does it have a pool?")
		yard = input("Does it have a yard?")
		result = {"pool": pool, "yard": yard}
		result.update(BaseProperty.prompt())
		return result


class Rental:
	def __init__(self, pre_paid, monthly_cost, **kwargs):
		super().__init__(**kwargs)
		self.pre_paid = pre_paid
		self.monthly_cost = monthly_cost
	
	@classmethod
	def prompt(cls):
		pre_paid = input("please enter pre paid amount :")
		monthly_cost = input("please enter monthly cost amount :")
		result = {"pre_paid": pre_paid, "monthly_cost": monthly_cost}
		return result


class PurchaseAble:
	def __init__(self, cost, **kwargs):
		self.cost = cost
		super().__init__(**kwargs)
	
	@classmethod
	def prompt(cls):
		cost = input("please enter cost amount:")
		result = {"cost": cost}
		return result
