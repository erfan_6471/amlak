
from estate import Apartment, Rental, House, PurchaseAble
from store import save_to_file


class ApartmentRental(Apartment, Rental):
	@classmethod
	def prompt(cls):
		result = Apartment.prompt()
		result.update(Rental.prompt())
		return result


class ApartmentPurchasable(Apartment, PurchaseAble):
	@classmethod
	def prompt(cls):
		result = Apartment.prompt()
		result.update(PurchaseAble.prompt())
		return result


class HouseRental(House, Rental):
	@classmethod
	def prompt(cls):
		result = House.prompt()
		result.update(Rental.prompt())
		return result


class HousePurchasable(House, PurchaseAble):
	@classmethod
	def prompt(cls):
		result = House.prompt()
		result.update(PurchaseAble.prompt())
		return result
