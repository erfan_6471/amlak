def check_access(func):
	def wrapped(obj, *args, **kwargs):
		if obj.has_access():
			return func(obj, *args, **kwargs)
		else:
			raise Exception("has access is not True")
	return wrapped
