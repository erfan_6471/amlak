from agent import Agent, SuperVisor
from constants import AGENTS_FILE_PATH
from store import load_data, save_to_file


def save_agent():
	agent = SuperVisor.create_agent()
	save_to_file(AGENTS_FILE_PATH, SuperVisor.agent_data())
	print("#" * 80)


def auth():
	agent_data = load_data(AGENTS_FILE_PATH)
	agents = [Agent(**d) for d in agent_data]
	agent = None
	while agent is None:
		user_name = input("please input username:")
		agent = SuperVisor.search(user_name)
	password = input("please enter password:")
	if agent.check_password(password):
		print("welcome")
	else:
		print("goodbye")



