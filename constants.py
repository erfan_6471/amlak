from profile import HousePurchasable, HouseRental, ApartmentPurchasable, ApartmentRental
from store import save_to_file

SUPERVISOR_CREDENTIALS = [{"username": "admin", "password": "123"}]
AGENTS_FILE_PATH = "H:\\project\\fixture\\agents.json"
PROFILE_MAPPER = {
	("house", "rent"): HouseRental,
	("apartment", "rent"): ApartmentRental
	, ("house", "purchase"): HousePurchasable
	, ("apartment", "purchase"): ApartmentPurchasable
}


def profile():
	request_house = input("please input your kind of house. house or apartment: ").lower()
	payment_type = input("please input your kind of money. rent or purchase: ").lower()
	request = (request_house, payment_type)
	
	try:
		ClassProfile = PROFILE_MAPPER[request]
	
	except KeyError:
		print("try again")
	
	else:
		return save_to_file("H:\\project\\fixture\\agents.json", ClassProfile.prompt())